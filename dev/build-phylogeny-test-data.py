#!/usr/bin/env python

import subprocess
import sys

from collections import defaultdict

from agalma import database as db
from biolite import catalog

data = defaultdict(list)

ids = subprocess.check_output(['shuf', '-n', '10', sys.argv[1]])
ids = ids.rstrip().split()
for id in set(ids):
	sql = """
		SELECT catalog_id, nucleotide_header, nucleotide_seq
		FROM sequences
		WHERE sequence_id=?;"""
	for row in db.execute(sql, (id,)):
		data[row[0]].append(">%s\n%s\n" % (row[1].partition(' ')[0], row[2]))

for key in data:
	print "Writing transcripts for", key
	open(key+'.fa', 'w').write(''.join(data[key]))
	print "Looking up paths for", key
	paths = catalog.split_paths(catalog.select(key).paths)
	if len(paths) == 2:
		print "Mapping reads for", key
		with open(key+'.log', 'w') as f:
			subprocess.check_call(['bowtie-build', key+'.fa', key],
				stdout=f, stderr=f)
			subprocess.check_call(['bowtie',
				'-q', '--phred33-quals', '-e', '99999999', '-l', '25',
				'-I', '1', '-X', '1200', '-p', '8', '-m', '200',
				key, '-1', paths[0], '-2', paths[1], '-S', '/dev/null',
				'--al', key+'.fq'],
				stdout=f, stderr=f)
	else:
		print "No reads to map for", key

# vim: noexpandtab ts=4 sw=4
