#!/bin/bash
#SBATCH -t 2:00:00
#SBATCH --mem=20G
export BLASTDB=/gpfs/scratch/shared/blastdb
set -e
blastdbcmd -db nt -entry all | python parse-nt-rrna.py | makeblastdb -dbtype nucl -in - -title "Agalma nt (rRNA subset) build $(date)" -out ../agalma/blastdb/nt.rrna
